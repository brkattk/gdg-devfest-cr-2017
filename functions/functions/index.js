const functions = require('firebase-functions')
const admin = require('firebase-admin')

admin.initializeApp(functions.config().firebase)

const ref = admin.database().ref()

exports.createUserAccount = functions.auth.user().onCreate(event => {
  const uid = event.data.uid
  const displayName = event.data.displayName || ""
  const email = event.data.email
  const photoUrl = event.data.photoUrl || 'https://firebasestorage.googleapis.com/v0/b/gdg-devfest-cr-2017.appspot.com/o/user.png?alt=media&token=c3f132ce-736e-4d6f-80a5-8782e6d561b4'

  const newUserRef = ref.child(`/users/${uid}`)
  return newUserRef.set({
    displayName: displayName,
    photoUrl: photoUrl,
    email: email
  })
})

exports.cleanupUserData = functions.auth.user().onDelete(event => {
  const uid = event.data.uid
  const userRef = ref.child(`/users/${uid}`)

  return userRef.update({isDeleted: true})
})